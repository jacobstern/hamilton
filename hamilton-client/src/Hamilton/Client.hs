{-# LANGUAGE ConstraintKinds     #-}
{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators       #-}

module Hamilton.Client
  ( createUser
  , getUser
  , getUsers
  , updateUser
  , generateUserToken
  , generateSuperuserToken
  , UserInfo(..)
  , User(..)
  , UserId(..)
  , Offset(..)
  , Limit(..)
  ) where

import           Adapter.API         (UsersAPI)
import qualified Auth
import           Core.Types
import           Data.ByteString     (ByteString)
import           Data.Proxy
import           Servant.API
import           Servant.Auth.Client
import           Servant.Auth.Server
import           Servant.Client

data ClientOps = ClientOps
  { opsCreateUser :: UserInfo -> ClientM User
  , opsGetUser    :: UserId -> ClientM User
  , opsGetUsers   :: Maybe Offset -> Maybe Limit -> ClientM [User]
  , opsUpdateUser :: UserId -> UserInfo -> ClientM User
  }

generateSuperuserToken :: ByteString -> IO Token
generateSuperuserToken secret =
  Token <$> Auth.generateSuperuserToken jwk
  where
    jwk = Auth.buildJWK secret

generateUserToken :: ByteString -> User -> IO Token
generateUserToken secret user =
  Token <$> Auth.generateUserToken jwk user
  where
    jwk = Auth.buildJWK secret

createUser :: Token -> UserInfo -> ClientM User
createUser = opsCreateUser . clientOps

getUser :: Token -> UserId -> ClientM User
getUser = opsGetUser . clientOps

getUsers :: Token -> Maybe Offset -> Maybe Limit -> ClientM [User]
getUsers = opsGetUsers . clientOps

updateUser :: Token -> UserId -> UserInfo -> ClientM User
updateUser = opsUpdateUser . clientOps

type ClientAPI = Auth '[ JWT] JwtClaims :> "users" :> UsersAPI

clientAPI :: Proxy ClientAPI
clientAPI = Proxy

clientOps :: Token -> ClientOps
clientOps token = ClientOps {..}
  where
    opsCreateUser :<|> opsGetUser :<|> opsGetUsers :<|> opsUpdateUser =
      client clientAPI token
