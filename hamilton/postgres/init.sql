create table users (
    id bigserial primary key not null,
    name text not null,
    avatar_url text,
    created_at timestamptz not null default now(),
    updated_at timestamptz not null default now()
);

create index users_created_at_idx on users (created_at);

create table rooms (
    id bigserial primary key not null,
    name text not null,
    created_at timestamptz not null default now(),
    updated_at timestamptz not null default now()
);

create index rooms_created_at_idx on rooms (created_at);

create table room_memberships (
    user_id bigserial references users(id),
    room_id bigserial references rooms(id),
    created_at timestamptz not null default now(),
    primary key (user_id, room_id)
);

create index room_memberships_created_at_idx on room_memberships (created_at);

create table events (
    id bigserial primary key not null,
    room_id bigserial references rooms(id),
    event_type text not null,
    created_at timestamptz not null default now(),
    updated_at timestamptz not null default now()
);

create index events_created_at_idx on events (created_at);

create table message_events (
    event_id bigserial primary key references events(id),
    sender_id bigserial references users(id),
    content text not null
);
