drop table if exists users cascade;
drop table if exists rooms cascade;
drop table if exists room_memberships cascade;
drop table if exists events cascade;
drop table if exists message_events cascade;
