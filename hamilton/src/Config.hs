{-# LANGUAGE OverloadedStrings #-}

module Config where

import           Data.ByteString (ByteString)
import           Data.Text       (Text)

data Config = Config
  { configDatabaseUrl         :: ByteString
  , configOpenConnectionCount :: Int
  , configJwtSecret           :: ByteString
  } deriving (Eq, Show)

devConfig :: Config
devConfig = Config
  { configDatabaseUrl = "postgresql://localhost:6212/hamilton"
  , configOpenConnectionCount = 10
  , configJwtSecret = "l31x2OkcP5OT3aCYeCfNAGJxAuo1GJT1"
  }
