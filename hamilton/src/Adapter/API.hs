{-# LANGUAGE ConstraintKinds       #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeOperators         #-}

module Adapter.API (makeApp, UsersAPI) where

import           Control.Monad.Except
import           Crypto.JOSE          (JWK)
import           Data.Aeson           (encode)
import           Data.Aeson.Types     (ToJSON)
import qualified Data.CaseInsensitive as CI
import           Data.Maybe
import           Servant
import           Servant.Auth.Server

import           Auth
import           Core.Services
import           Core.Types

type App m = AllRepo m

paginationFromQuery :: Maybe Offset -> Maybe Limit -> Pagination
paginationFromQuery offset limit = Pagination
  { offset = fromMaybe defaultOffset offset
  , limit = fromMaybe defaultLimit limit
  }
  where
    defaultOffset = Offset 0
    defaultLimit = Limit 50

-- * Routes

type API = Auth '[JWT] JwtClaims :> ProtectedAPI

server :: App m => ServerT API (ExceptT ServantErr m)
server = protectedServer

type ProtectedAPI = "users" :> UsersAPI :<|> "rooms" :> RoomsAPI

protectedServer
  :: App m
  => AuthResult JwtClaims -> ServerT ProtectedAPI (ExceptT ServantErr m)
protectedServer (Authenticated claims) =
  usersServer claims :<|> roomsServer claims
protectedServer _ = throwAll err401 {errBody = "Invalid or missing credentials"}

type UsersAPI = ReqBody '[JSON] UserInfo :> Post '[JSON] User
           :<|> Capture "id" UserId :> Get '[JSON] User
           :<|> QueryParam "offset" Offset :> QueryParam "limit" Limit :> Get '[JSON] [User]
           :<|> Capture "id" UserId :> ReqBody '[JSON] UserInfo :> Put '[JSON] User

usersServer :: App m => JwtClaims -> ServerT UsersAPI (ExceptT ServantErr m)
usersServer claims =
  handlePostUsers claims :<|> handleGetUsersId claims :<|> handleGetUsers claims :<|> handlePutUsersId claims

handlePostUsers :: App m => JwtClaims -> UserInfo -> ExceptT ServantErr m User
handlePostUsers claims user = wrapService $ createUser claims user

handleGetUsersId :: App m => JwtClaims -> UserId -> ExceptT ServantErr m User
handleGetUsersId claims userId = wrapService $ getUser claims userId

handleGetUsers
  :: App m
  => JwtClaims -> Maybe Offset -> Maybe Limit -> ExceptT ServantErr m [User]
handleGetUsers claims offset limit =
  wrapService $ getUsers claims $ paginationFromQuery offset limit

handlePutUsersId
  :: App m
  => JwtClaims -> UserId -> UserInfo -> ExceptT ServantErr m User
handlePutUsersId claims userId user =
  wrapService $ updateUser claims userId user

type RoomsAPI = ReqBody '[JSON] RoomInfo :> Post '[JSON] Room
           :<|> Capture "id" RoomId :> Get '[JSON] Room
           :<|> QueryParam "offset" Offset :> QueryParam "limit" Limit :> Get '[JSON] [Room]
           :<|> Capture "id" RoomId :> ReqBody '[JSON] RoomInfo :> Put '[JSON] Room
           :<|> Capture "id" RoomId :> "members" :> RoomsIdMembersAPI
           :<|> Capture "id" RoomId :> "membership" :> RoomsIdMembershipAPI
           :<|> Capture "id" RoomId :> "messages" :> RoomsIdMessagesAPI

roomsServer :: App m => JwtClaims -> ServerT RoomsAPI (ExceptT ServantErr m)
roomsServer claims =
       handlePostRooms claims
  :<|> handleGetRoomsId claims
  :<|> handleGetRooms claims
  :<|> handlePutRoomsId claims
  :<|> handleGetRoomsIdMembers claims
  :<|> roomsIdMembershipServer claims
  :<|> roomsIdMessagesServer claims

handlePostRooms :: App m => JwtClaims -> RoomInfo -> ExceptT ServantErr m Room
handlePostRooms claims room = wrapService $ createRoom claims room

handleGetRoomsId :: App m => JwtClaims -> RoomId -> ExceptT ServantErr m Room
handleGetRoomsId claims roomId = wrapService $ getRoom claims roomId

handleGetRooms
  :: App m
  => JwtClaims -> Maybe Offset -> Maybe Limit -> ExceptT ServantErr m [Room]
handleGetRooms claims offset limit =
  wrapService $ getRooms claims $ paginationFromQuery offset limit

handlePutRoomsId
  :: App m
  => JwtClaims -> RoomId -> RoomInfo -> ExceptT ServantErr m Room
handlePutRoomsId claims roomId room =
  wrapService $ updateRoom claims roomId room

type RoomsIdMembersAPI = QueryParam "offset" Offset :> QueryParam "limit" Limit :> Get '[JSON] [User]

handleGetRoomsIdMembers
  :: App m
  => JwtClaims -> RoomId -> Maybe Offset -> Maybe Limit -> ExceptT ServantErr m [User]
handleGetRoomsIdMembers claims roomId offset limit =
  wrapService $ getRoomMembers claims roomId $ paginationFromQuery offset limit

type RoomsIdMembershipAPI = PostNoContent '[JSON] NoContent
                       :<|> DeleteNoContent '[JSON] NoContent

roomsIdMembershipServer
  :: App m
  => JwtClaims -> RoomId -> ServerT RoomsIdMembershipAPI (ExceptT ServantErr m)
roomsIdMembershipServer claims roomId =
       handlePostRoomsIdMembership claims roomId
  :<|> handleDeleteRoomsIdMembership claims roomId

handlePostRoomsIdMembership
  :: App m
  => JwtClaims -> RoomId -> ExceptT ServantErr m NoContent
handlePostRoomsIdMembership claims roomId =
  wrapService $ joinRoom claims roomId >> return NoContent

handleDeleteRoomsIdMembership
  :: App m
  => JwtClaims -> RoomId -> ExceptT ServantErr m NoContent
handleDeleteRoomsIdMembership claims roomId =
  wrapService $ leaveRoom claims roomId >> return NoContent

type RoomsIdMessagesAPI =
       ReqBody '[JSON] MessageInfo :> Post '[JSON] Message
  :<|> QueryParam "offset" Offset :> QueryParam "limit" Limit :> Get '[JSON] [Message]

roomsIdMessagesServer
  :: App m
  => JwtClaims -> RoomId -> ServerT RoomsIdMessagesAPI (ExceptT ServantErr m)
roomsIdMessagesServer claims roomId =
       handlePostRoomsIdMessages claims roomId
  :<|> handleGetRoomsIdMessages claims roomId

handlePostRoomsIdMessages
  :: App m
  => JwtClaims -> RoomId -> MessageInfo -> ExceptT ServantErr m Message
handlePostRoomsIdMessages claims roomId message =
  wrapService $ sendMessage claims roomId message

handleGetRoomsIdMessages
  :: App m
  => JwtClaims -> RoomId -> Maybe Offset -> Maybe Limit -> ExceptT ServantErr m [Message]
handleGetRoomsIdMessages claims roomId offset limit =
  wrapService $ getMessages claims roomId $ paginationFromQuery offset limit

-- * Errors

serviceErrorServantErr :: ServiceError -> ServantErr
serviceErrorServantErr error = case error of
  UserNotFound -> err404 {errBody = "User not found"}
  RoomNotFound -> err404 {errBody = "Room not found"}
  Forbidden    -> err403 {errBody = "Forbidden"}

wrapService
  :: Monad m
  => ExceptT ServiceError m a -> ExceptT ServantErr m a
wrapService = withExceptT serviceErrorServantErr

-- * Launching the app

api :: Proxy API
api = Proxy

context :: Proxy '[ CookieSettings, JWTSettings]
context = Proxy

makeApp :: App m => (forall x. m x -> IO x) -> JWK -> Application
makeApp runner jwk = serveWithContext api values hoisted
  where
    values = defaultCookieSettings :. jwtSettings jwk :. EmptyContext
    go = Handler . ExceptT . runner . runExceptT
    hoisted = hoistServerWithContext api context go server
