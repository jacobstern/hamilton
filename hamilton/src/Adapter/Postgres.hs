{-# LANGUAGE ConstraintKinds       #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE QuasiQuotes           #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}

module Adapter.Postgres where

import           Control.Monad.Reader
import           Data.ByteString                  (ByteString)
import           Data.Has
import           Data.Int
import           Data.Maybe
import           Data.Pool
import           Data.Time                        (UTCTime)
import           Database.PostgreSQL.Simple
import           Database.PostgreSQL.Simple.SqlQQ
import           Database.PostgreSQL.Simple.Types

import           Core.Types

type PostgresPool = Pool Connection

--instance FromRow User where
--  fromRow = User <$> field <*> field <*> field
--
--instance FromRow Room where
--  fromRow = Room <$> field <*> field
--
--instance FromRow Message where
--  fromRow = Message <$> field <*> fromRow <*> field <*> field

type RoomMembership = (UserId, RoomId, UTCTime)

createPostgresPool :: ByteString -> Int -> IO PostgresPool
createPostgresPool connectionString =
  createPool (connectPostgreSQL connectionString) close 1 20

type Postgres r m = (MonadReader r m, Has PostgresPool r, MonadIO m)

runQuery :: (Postgres r m, ToRow p, FromRow r') => Query -> p -> m [r']
runQuery q params = do
  pool <- asks getter
  liftIO . withResource pool $ \conn -> query conn q params

runExecute :: (Postgres r m, ToRow p) => Query -> p -> m Int64
runExecute q params = do
  pool <- asks getter
  liftIO . withResource pool $ \conn -> execute conn q params

createUser :: Postgres r m => UserInfo -> m User
createUser UserInfo {..} =
  head <$> runQuery
    [sql| insert into users (name, avatar_url)
          values (?, ?)
          returning id, name, avatar_url |]
    (name, avatarUrl)

getUser :: Postgres r m => UserId -> m (Maybe User)
getUser userId =
  listToMaybe <$> runQuery
    [sql| select id, name, avatar_url from users
          where id = ? |]
    (Only userId)

getUsers :: Postgres r m => Pagination -> m [User]
getUsers Pagination {..} =
  runQuery
    [sql| select id, name, avatar_url from users
          order by created_at desc limit ? offset ? |]
    (limit, offset)

updateUser :: Postgres r m  => UserId -> UserInfo -> m (Maybe User)
updateUser userId UserInfo {..} =
  listToMaybe <$> runQuery
    [sql| update users set name = ?, avatar_url = ?, updated_at = now()
          where id = ?
          returning id, name, avatar_url |]
    (name, avatarUrl, userId)

createRoom :: Postgres r m => RoomInfo -> m Room
createRoom RoomInfo {..} =
  head <$> runQuery
    [sql| insert into rooms (name)
          values (?)
          returning id, name |]
    (Only name)

getRoom :: Postgres r m => RoomId -> m (Maybe Room)
getRoom roomId =
  listToMaybe <$> runQuery
    [sql| select id, name from rooms
          where id = ? |]
    (Only roomId)

getRooms :: Postgres r m => Pagination -> m [Room]
getRooms Pagination {..} =
  runQuery
    [sql| select id, name from rooms
          order by created_at desc limit ? offset ? |]
    (limit, offset)

updateRoom :: Postgres r m => RoomId -> RoomInfo -> m (Maybe Room)
updateRoom roomId RoomInfo {..} =
  listToMaybe <$> runQuery
    [sql| update rooms set name = ?, updated_at = now()
          where id = ?
          returning id, name |]
    (name, roomId)

getRoomMembers :: Postgres r m => RoomId -> Pagination -> m [User]
getRoomMembers roomId Pagination {..} =
  runQuery
    [sql| select u.id, u.name, u.avatar_url from users as u
          inner join room_memberships as rm on rm.user_id = u.id
          where rm.room_id = ?
          order by rm.created_at desc limit ? offset ? |]
    (roomId, limit, offset)

isRoomMember :: Postgres r m => UserId -> RoomId -> m Bool
isRoomMember userId roomId =
  notEmpty <$> runQuery
    [sql| select * from room_memberships where user_id = ? and room_id = ? |]
    (userId, roomId)
  where
    notEmpty = not . null :: [RoomMembership] -> Bool

addToRoom :: Postgres r m => UserId -> RoomId -> m ()
addToRoom userId roomId =
  void $ runExecute
    [sql| insert into room_memberships (user_id, room_id)
          values (?, ?) |]
    (userId, roomId)

removeFromRoom :: Postgres r m => UserId -> RoomId -> m ()
removeFromRoom userId roomId =
  void $ runExecute
    [sql| delete from room_memberships
          where user_id = ? and room_id = ? |]
    (userId, roomId)

createMessage :: Postgres r m => UserId -> RoomId -> MessageInfo -> m Message
createMessage senderId roomId MessageInfo {..} =
  head <$> runQuery
    [sql| with e as (
            insert into events (room_id, event_type)
            values (?, 'message')
            returning id, created_at
          ), m as (
            insert into message_events (event_id, sender_id, content)
            select id, ?, ?
            from e
            returning sender_id, content
          )
          select e.id, u.id, u.name, u.avatar_url, m.content, e.created_at
          from users as u, e, m
          where u.id = m.sender_id
          order by e.created_at desc |]
    (roomId, senderId, content)

getMessages :: Postgres r m => RoomId -> Pagination -> m [Message]
getMessages roomId (Pagination offset limit) =
  runQuery
    [sql| select e.id, u.id, u.name, u.avatar_url, m.content, e.created_at
          from events as e
          inner join message_events as m on m.event_id = e.id
          inner join users as u on u.id = m.sender_id
          where e.room_id = ?
          order by e.created_at desc limit ? offset ? |]
    (roomId, limit, offset)
