{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TypeFamilies               #-}

module Lib
  ( startApp
  , makeApp
  ) where

import           Control.Monad.Base
import           Control.Monad.Catch
import           Control.Monad.IO.Class      (liftIO)
import           Control.Monad.Reader
import           Data.ByteString             (ByteString)
import           Data.Maybe
import           Data.Pool                   (Pool)
import           Network.Wai
import           Network.Wai.Handler.Warp
import           Servant.Auth.Server         (defaultJWTSettings)
import           System.Environment

import Auth
import qualified Adapter.API                 as API
import qualified Adapter.Postgres             as PG
import           Config
import           Core.Types

type Env = PG.PostgresPool

newtype Hamilton a = Hamilton
  { unHamiltonT :: ReaderT Env IO a
  } deriving ( Applicative
             , Functor
             , Monad
             , MonadIO
             , MonadThrow
             , MonadCatch
             , MonadReader Env
             )

runner :: Env -> Hamilton r -> IO r
runner env hamilton = flip runReaderT env $ unHamiltonT hamilton

makeApp :: Config -> IO Application
makeApp config@Config { configDatabaseUrl = databaseUrl
                      , configOpenConnectionCount = openConnectionCount
                      , configJwtSecret = jwtSecret
                      } = do
  pool <- PG.createPostgresPool databaseUrl openConnectionCount
  return $ API.makeApp (runner pool) $ buildJWK jwtSecret

startApp :: IO ()
startApp = do
  port <- read . fromMaybe "1234" <$> lookupEnv "PORT"
  makeApp config >>= run port
  where
    config = devConfig

instance UserRepo Hamilton where
  repoCreateUser = PG.createUser
  repoGetUser = PG.getUser
  repoGetUsers = PG.getUsers
  repoUpdateUser = PG.updateUser

instance RoomRepo Hamilton where
  repoCreateRoom = PG.createRoom
  repoGetRoom = PG.getRoom
  repoGetRooms = PG.getRooms
  repoUpdateRoom = PG.updateRoom

instance RoomMembershipRepo Hamilton where
  repoGetRoomMembers = PG.getRoomMembers
  repoIsRoomMember = PG.isRoomMember
  repoAddToRoom = PG.addToRoom
  repoRemoveFromRoom = PG.removeFromRoom

instance MessageRepo Hamilton where
  repoCreateMessage = PG.createMessage
  repoGetMessages = PG.getMessages
