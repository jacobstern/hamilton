{-# LANGUAGE NamedFieldPuns #-}

module Auth where

import           Crypto.JOSE
import           Crypto.JOSE.Types
import           Data.ByteString         (ByteString)
import qualified Data.ByteString.Lazy    as BL
import           Data.Monoid
import qualified Data.Text               as T
import           Servant.Auth.Server

import           Core.Types

type Token = ByteString

jwtSettings :: JWK -> JWTSettings
jwtSettings = defaultJWTSettings

generateToken :: JWK -> JwtClaims -> IO Token
generateToken jwk claims =
  either failure success =<< makeJWT claims jwtSettings Nothing
  where
    jwtSettings = defaultJWTSettings jwk
    failure e = error $ "Error generating JWT: " <> show e
    success = return . BL.toStrict

generateUserToken :: JWK -> User -> IO Token
generateUserToken jwk user@User {userId} =
  generateToken jwk claims
  where
    claims = JwtClaims {sub = Just userId, superuser = Nothing}

generateSuperuserToken :: JWK -> IO Token
generateSuperuserToken jwk =
  generateToken jwk claims
  where
    claims = JwtClaims {sub = Nothing, superuser = Just True}

buildJWK :: ByteString -> JWK
buildJWK secret = go octets
  where
    go = fromKeyMaterial . OctKeyMaterial . OctKeyParameters
    octets = Base64Octets secret
