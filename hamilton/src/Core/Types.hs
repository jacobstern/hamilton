{-# LANGUAGE ConstraintKinds            #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DuplicateRecordFields      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Core.Types where

import           Data.Aeson                           (FromJSON, ToJSON,
                                                       parseJSON, toJSON)
import           Data.Int
import           Data.Text                            (Text)
import           Data.Time                            (UTCTime)
import           Database.PostgreSQL.Simple.FromField
import           Database.PostgreSQL.Simple.FromRow
import           Database.PostgreSQL.Simple.ToField
import           GHC.Generics
import           Servant                              (FromHttpApiData, ToHttpApiData)
import           Servant.Auth.Server                  (FromJWT, ToJWT)

-- * API types

newtype UserId = UserId
  { getUserId :: Int
  } deriving (Eq, Show, ToJSON, FromJSON, ToField, FromField, FromHttpApiData, ToHttpApiData)

type Url = Text

data User = User
  { userId    :: UserId
  , name      :: Text
  , avatarUrl :: Maybe Url
  } deriving (Eq, Show, Generic)

instance ToJSON User

instance FromJSON User

instance FromRow User

data UserInfo = UserInfo
  { name      :: Text
  , avatarUrl :: Maybe Url
  } deriving (Eq, Show, Generic)

instance ToJSON UserInfo

instance FromJSON UserInfo

newtype Offset = Offset
  { getOffset :: Int
  } deriving (Eq, Show, ToJSON, FromJSON, ToField, FromHttpApiData, ToHttpApiData)

newtype Limit = Limit
  { getLimit :: Int
  } deriving (Eq, Show, ToJSON, FromJSON, ToField, FromHttpApiData, ToHttpApiData)

data Pagination = Pagination
  { offset :: Offset
  , limit  :: Limit
  } deriving (Eq, Show, Generic)

instance ToJSON Pagination

instance FromJSON Pagination

newtype RoomId = RoomId
  { getRoomId :: Int
  } deriving (Eq, Show, ToJSON, FromJSON, ToField, FromField, FromHttpApiData, ToHttpApiData)

data Room = Room
  { roomId :: RoomId
  , name   :: Text
  } deriving (Eq, Show, Generic)

instance ToJSON Room

instance FromJSON Room

instance FromRow Room

newtype RoomInfo = RoomInfo
  { name :: Text
  } deriving (Eq, Show, Generic)

instance ToJSON RoomInfo

instance FromJSON RoomInfo

newtype MessageId = MessageId
  { getMessageId :: Int
  } deriving (Eq, Show, ToJSON, FromJSON, ToField, FromField, FromHttpApiData, ToHttpApiData)

data Message = Message
  { messageId :: MessageId
  , sender    :: User
  , content   :: Text
  , timestamp :: UTCTime
  } deriving (Eq, Show, Generic)

instance ToJSON Message

instance FromJSON Message

instance FromRow Message where
  fromRow = Message <$> field <*> fromRow <*> field <*> field

newtype MessageInfo = MessageInfo
  { content :: Text
  } deriving (Eq, Show, Generic)

instance ToJSON MessageInfo

instance FromJSON MessageInfo

-- * Auth

data JwtClaims = JwtClaims
  { sub       :: Maybe UserId
  , superuser :: Maybe Bool
  } deriving (Eq, Show, Generic)

instance ToJSON JwtClaims

instance FromJSON JwtClaims

instance ToJWT JwtClaims

instance FromJWT JwtClaims

-- * MTL type classes

class Monad m => UserRepo m where
  repoCreateUser :: UserInfo -> m User
  repoGetUser :: UserId -> m (Maybe User)
  repoGetUsers :: Pagination -> m [User]
  repoUpdateUser :: UserId -> UserInfo -> m (Maybe User)

class Monad m => RoomRepo m where
  repoCreateRoom :: RoomInfo -> m Room
  repoGetRoom :: RoomId -> m (Maybe Room)
  repoGetRooms :: Pagination -> m [Room]
  repoUpdateRoom :: RoomId -> RoomInfo -> m (Maybe Room)

class Monad m => RoomMembershipRepo m where
  repoGetRoomMembers :: RoomId -> Pagination -> m [User]
  repoIsRoomMember :: UserId -> RoomId -> m Bool
  repoAddToRoom :: UserId -> RoomId -> m ()
  repoRemoveFromRoom :: UserId -> RoomId -> m ()

class Monad m => MessageRepo m where
  repoCreateMessage :: UserId -> RoomId -> MessageInfo -> m Message
  repoGetMessages :: RoomId -> Pagination -> m [Message]

type AllRepo m = (UserRepo m, RoomRepo m, RoomMembershipRepo m, MessageRepo m)
