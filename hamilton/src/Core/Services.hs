module Core.Services where

import           Control.Monad.Except

import           Core.Authorization
import           Core.Types

data ServiceError = UserNotFound | Forbidden | RoomNotFound

createUser
  :: UserRepo m
  => JwtClaims -> UserInfo -> ExceptT ServiceError m User
createUser claims user = do
  authorize $ authorizeUserAccess CreateAccess claims
  lift $ repoCreateUser user

getUser :: UserRepo m => JwtClaims -> UserId -> ExceptT ServiceError m User
getUser claims userId = do
  authorize $ authorizeUserAccess ReadAccess claims
  found <- lift $ repoGetUser userId
  case found of
    Just user -> return user
    Nothing   -> throwError UserNotFound

getUsers :: UserRepo m => JwtClaims -> Pagination -> ExceptT ServiceError m [User]
getUsers claims pagination = do
  authorize $ authorizeUserAccess ListAccess claims
  lift $ repoGetUsers pagination

updateUser
  :: UserRepo m
  => JwtClaims -> UserId -> UserInfo -> ExceptT ServiceError m User
updateUser claims userId user = do
  authorize $ authorizeUserAccess UpdateAccess claims
  updated <- lift $ repoUpdateUser userId user
  case updated of
    Just user -> return user
    Nothing   -> throwError UserNotFound

createRoom
  :: RoomRepo m
  => JwtClaims -> RoomInfo -> ExceptT ServiceError m Room
createRoom claims room = lift $ repoCreateRoom room

getRoom :: RoomRepo m => JwtClaims -> RoomId -> ExceptT ServiceError m Room
getRoom claims roomId = do
  found <- lift $ repoGetRoom roomId
  case found of
    Just room -> return room
    Nothing   -> throwError RoomNotFound

getRooms
  :: RoomRepo m
  => JwtClaims -> Pagination -> ExceptT ServiceError m [Room]
getRooms claims pagination = lift $ repoGetRooms pagination

updateRoom
  :: RoomRepo m
  => JwtClaims -> RoomId -> RoomInfo -> ExceptT ServiceError m Room
updateRoom claims roomId room = do
  updated <- lift $ repoUpdateRoom roomId room
  case updated of
    Just room -> return room
    Nothing   -> throwError RoomNotFound

getRoomMembers
  :: RoomMembershipRepo m
  => JwtClaims -> RoomId -> Pagination -> ExceptT ServiceError m [User]
getRoomMembers claims roomId pagination =
  lift $ repoGetRoomMembers roomId pagination

joinRoom
  :: RoomMembershipRepo m
  => JwtClaims -> RoomId -> ExceptT ServiceError m ()
joinRoom claims roomId = withUser claims $ \userId -> do
  isRoomMember <- lift $ repoIsRoomMember userId roomId
  unless isRoomMember $ lift $ repoAddToRoom userId roomId

leaveRoom
  :: RoomMembershipRepo m
  => JwtClaims -> RoomId -> ExceptT ServiceError m ()
leaveRoom claims roomId = withUser claims $ \userId -> do
  isRoomMember <- lift $ repoIsRoomMember userId roomId
  when isRoomMember $ lift $ repoRemoveFromRoom userId roomId

sendMessage
  :: MessageRepo m
  => JwtClaims -> RoomId -> MessageInfo -> ExceptT ServiceError m Message
sendMessage claims roomId message = withUser claims $ \userId ->
  lift $ repoCreateMessage userId roomId message

getMessages
  :: MessageRepo m
  => JwtClaims -> RoomId -> Pagination -> ExceptT ServiceError m [Message]
getMessages claims roomId pagination = lift $ repoGetMessages roomId pagination

-- * Helpers

authorize :: Monad m => AuthorizatonStatus -> ExceptT ServiceError m ()
authorize Authorized    = return ()
authorize NotAuthorized = throwError Forbidden

withUser
  :: Monad m
  => JwtClaims
  -> (UserId -> ExceptT ServiceError m r)
  -> ExceptT ServiceError m r
withUser claims action = case claims of
   JwtClaims {sub = Just userId} -> action userId
   _                             -> throwError UserNotFound
