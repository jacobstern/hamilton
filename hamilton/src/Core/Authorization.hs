{-# LANGUAGE LambdaCase #-}

module Core.Authorization where

import           Core.Types

data ResourceAccessType =
  CreateAccess | ListAccess | ReadAccess | UpdateAccess | DeleteAccess
  deriving (Eq, Show)

data AuthorizatonStatus = Authorized | NotAuthorized
  deriving (Eq, Show)

authorizeUserAccess :: ResourceAccessType -> JwtClaims -> AuthorizatonStatus
authorizeUserAccess ReadAccess = authorizeAny
authorizeUserAccess _          = authorizeSuperuser

authorizeSuperuser :: JwtClaims -> AuthorizatonStatus
authorizeSuperuser = \case
  JwtClaims {superuser = Just True} -> Authorized
  _                                 -> NotAuthorized

authorizeAny :: JwtClaims -> AuthorizatonStatus
authorizeAny = \case
  JwtClaims {sub = Just _}       -> Authorized
  JwtClaims {superuser = Just _} -> Authorized
  _                              -> NotAuthorized
