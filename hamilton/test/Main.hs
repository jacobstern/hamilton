{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Control.Exception                (bracket, finally)
import           Control.Monad                    (void)
import qualified Data.ByteString                  as BS
import           Database.PostgreSQL.Simple
import           Database.PostgreSQL.Simple.SqlQQ (sql)
import           Database.PostgreSQL.Simple.Types (Query (..))
import qualified Spec
import           Test.Hspec.Runner

main :: IO ()
main = do
  executeSqlFile "postgres/init.sql"
  finally test teardown
  where
    test = hspecWith defaultConfig Spec.spec
    teardown = executeSqlFile "postgres/teardown.sql"

executeSqlFile :: String -> IO ()
executeSqlFile filePath = do
  contents <- BS.readFile filePath
  let executeQuery conn = void $ execute_ conn $ Query contents
  bracket acquire release executeQuery
  where
    acquire = connectPostgreSQL "postgresql://localhost:6212/hamiltontest"
    release = close
