{-# LANGUAGE OverloadedStrings #-}

module Test.Helpers.API where

import           Auth
import           Config
import           Control.Exception.Base
import           Control.Lens
import           Control.Monad
import           Control.Monad.Catch
import           Control.Monad.IO.Class
import           Core.Types
import           Data.Aeson
import           Data.ByteString.Lazy     as BL
import           Data.Monoid
import           Data.Text.Encoding       (encodeUtf8)
import           Lib                      (makeApp)
import           Network.HTTP.Client      (HttpException (..),
                                           HttpExceptionContent (..))
import           Network.Wai.Handler.Warp
import           Network.Wreq
import           Test.Hspec

data ClientEnv = ClientEnv
  { clientEnvBaseUrl        :: String
  , clientEnvSuperuserToken :: Token
  }

data ClientEnvWithUser = ClientEnvWithUser
  { clientEnvWithUserBaseUrl        :: String
  , clientEnvWithUserSuperuserToken :: Token
  , clientEnvWithUserUser           :: User
  , clientEnvWithUserToken          :: Token
  }

getUser :: ClientEnvWithUser -> User
getUser = clientEnvWithUserUser

class EnvHelpers a where
  buildUrl :: a -> String -> String
  superuserOpts :: a -> Options

instance EnvHelpers ClientEnv where
  buildUrl (ClientEnv baseUrl _) = appendUrl baseUrl
  superuserOpts (ClientEnv _ token) = tokenOpts token

instance EnvHelpers ClientEnvWithUser where
  buildUrl (ClientEnvWithUser baseUrl _ _ _) = appendUrl baseUrl
  superuserOpts (ClientEnvWithUser _ token _ _) = tokenOpts token

appendUrl :: String -> String -> String
appendUrl = mappend

tokenOpts :: Token -> Options
tokenOpts token = defaults & header "Authorization" .~ [authorization]
  where
    authorization = encodeUtf8 $ "Bearer " <> token

userOpts :: ClientEnvWithUser -> Options
userOpts ClientEnvWithUser {clientEnvWithUserToken = token} = tokenOpts token

testConfig :: Config
testConfig =
  Config
  { configOpenConnectionCount = 10
  , configDatabaseUrl = "postgresql://localhost:6212/hamiltontest"
  , configJwtSecret = "l31x2OkcP5OT3aCYeCfNAGJxAuo1GJT1"
  }

withApp :: ActionWith ClientEnv -> IO ()
withApp action = do
  superuserToken <- generateSuperuserToken jwk
  withApplication (makeApp config) $ \port ->
    action $ ClientEnv ("http://localhost:" <> show port) superuserToken
  where
    config@Config {configJwtSecret = secret} = testConfig
    jwk = buildJWK secret

withAuthenticatedUser :: ActionWith ClientEnvWithUser -> ActionWith ClientEnv
withAuthenticatedUser action env@(ClientEnv baseUrl superuserToken) = do
  let create = UserInfo "Authenticated User" Nothing
      url = buildUrl env "/users"
      opts = superuserOpts env
  user <- requestJSON $ postWith opts url $ toJSON create
  userToken <- generateUserToken jwk user
  action $ ClientEnvWithUser baseUrl superuserToken user userToken
  where
    jwk = buildJWK $ configJwtSecret testConfig

isStatusCodeException :: Int -> HttpException -> Bool
isStatusCodeException code (HttpExceptionRequest _ (StatusCodeException r _)) =
  r ^. responseStatus . statusCode == code
isStatusCodeException _ _ = False

requestJSON ::
     (MonadThrow m, MonadIO m, FromJSON a) => IO (Response BL.ByteString) -> m a
requestJSON req = do
  response <- liftIO $ asJSON =<< req
  return $ response ^. responseBody
