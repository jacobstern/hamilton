{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}

module UsersSpec where

import           Core.Types
import           Data.Aeson
import           Data.Monoid
import           Network.Wreq
import           Test.Helpers.API
import           Test.Hspec

spec :: Spec
spec = around withApp $ do
  describe "/users" $ do
    it "should add a new user" $ \env -> do
      let avatarUrl = Just "https://www.gravatar.com/avatar/00000000000000000000000000000000"
          create = UserInfo "Perry" avatarUrl
          url = buildUserListUrl env
          opts = superuserOpts env
      user@User {userId} <- requestJSON $ postWith opts url $ toJSON create
      user `shouldBe` User userId "Perry" avatarUrl

    it "should retrieve users in descending created order" $ \env -> do
      let url = buildUserListUrl env
          opts = superuserOpts env
      user1 <- requestJSON $ postWith opts url $ toJSON $ UserInfo "1" Nothing
      user2 <- requestJSON $ postWith opts url $ toJSON $ UserInfo "2" Nothing
      users <- requestJSON $ getWith opts url
      users `shouldStartWith` [user2 :: User, user1]

    aroundWith withAuthenticatedUser $ do
      it "should not authorize user creation for non-superuser" $ \env -> do
        let create = UserInfo "Elmer" Nothing
            url = buildUserListUrl env
            opts = userOpts env
            createRequest = postWith opts url $ toJSON create
        createRequest `shouldThrow` isStatusCodeException 403

      it "should not authorize reading the user list for non-superuser" $ \env -> do
        let url = buildUserListUrl env
            opts = userOpts env
            readRequest = getWith opts url
        readRequest `shouldThrow` isStatusCodeException 403

  describe "/users/:user_id" $ do
    it "should retrieve a single user" $ \env -> do
      let avatarUrl = Just "https://www.gravatar.com/avatar/00000000000000000000000000000001"
          create = UserInfo "Patrick" avatarUrl
          url = buildUserListUrl env
          opts = superuserOpts env
      user@User {userId} <- requestJSON $ postWith opts url $ toJSON create
      let getRequest = requestJSON $ getWith opts $ buildUserUrl env userId
      getRequest `shouldReturn` user

    it "should update a user" $ \env -> do
      let create = UserInfo "Littlefinger" Nothing
          url = buildUserListUrl env
          opts = superuserOpts env
      user@User {userId} <- requestJSON $ postWith opts url $ toJSON create
      let avatarUrl = Just "https://www.gravatar.com/avatar/00000000000000000000000000000002"
          update = UserInfo "Sansa" avatarUrl
          roomUrl = buildUserUrl env userId
          updateRequest = requestJSON $ putWith opts roomUrl $ toJSON update
          expectedAfterUpdates = User userId "Sansa" avatarUrl
      updateRequest `shouldReturn` expectedAfterUpdates
      rooms <- requestJSON $ getWith opts url
      rooms `shouldStartWith` [expectedAfterUpdates]

    aroundWith withAuthenticatedUser $ do
      it "should authorize accessing a single user for non-superuser" $ \env -> do
        let avatarUrl = Just "https://www.gravatar.com/avatar/00000000000000000000000000000003"
            create = UserInfo "Patreon" avatarUrl
            url = buildUserListUrl env
            createOpts = superuserOpts env
            getOpts = userOpts env
        user@User {userId} <- requestJSON $ postWith createOpts url $ toJSON create
        let getRequest = requestJSON $ getWith getOpts $ buildUserUrl env userId
        getRequest `shouldReturn` user

      it "should not authorize updating a user for non-superuser" $ \env -> do
        let updates = UserInfo "Uncle" Nothing
            url = buildUserListUrl env
            opts = userOpts env
            User {userId} = clientEnvWithUserUser env
            userUrl = buildUserUrl env userId
            updateRequest = putWith opts userUrl $ toJSON updates
        updateRequest `shouldThrow` isStatusCodeException 403

buildUserListUrl :: EnvHelpers e => e -> String
buildUserListUrl = flip buildUrl "/users"

buildUserUrl :: EnvHelpers e => e -> UserId -> String
buildUserUrl env (UserId wrapped) = buildUserListUrl env <> "/" <> show wrapped
