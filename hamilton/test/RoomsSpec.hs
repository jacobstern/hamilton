{-# LANGUAGE NamedFieldPuns      #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module RoomsSpec where

import           Core.Types
import           Data.Aeson
import qualified Data.ByteString as BS
import           Data.Monoid
import           Network.Wreq
import           Test.Helpers.API
import           Test.Hspec

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

spec :: Spec
spec = around withApp $ do
  describe "/rooms" $ do
    it "should add a new room" $ \env -> do
      let create = RoomInfo "Grotto"
          url = buildRoomListUrl env
          opts = superuserOpts env
      room@Room {roomId} <- requestJSON $ postWith opts url $ toJSON create
      room `shouldBe` Room roomId "Grotto"

    it "should retrieve rooms in descending created order" $ \env -> do
      let url = buildRoomListUrl env
          opts = superuserOpts env
      room1 <- requestJSON $ postWith opts url $ toJSON $ RoomInfo "Casterly"
      room2 <- requestJSON $ postWith opts url $ toJSON $ RoomInfo "Aerie"
      rooms <- requestJSON $ getWith opts url
      rooms `shouldStartWith` [room2 :: Room, room1]

  describe "/rooms/:room_id" $ do
    it "should retrieve a single room" $ \env -> do
      let create = RoomInfo "Hearth"
          url = buildRoomListUrl env
          opts = superuserOpts env
      room@Room {roomId} <- requestJSON $ postWith opts url $ toJSON create
      let getRequest = requestJSON $ getWith opts $ buildRoomUrl env roomId
      getRequest `shouldReturn` room

    it "should update a room" $ \env -> do
      let create = RoomInfo "The Shire"
          url = buildRoomListUrl env
          opts = superuserOpts env
      Room {roomId} <- requestJSON $ postWith opts url $ toJSON create
      let update = RoomInfo "Hobbiton"
          roomUrl = buildRoomUrl env roomId
          updateRequest = requestJSON $ putWith opts roomUrl $ toJSON update
          expectedAfterUpdates = Room roomId "Hobbiton"
      updateRequest `shouldReturn` expectedAfterUpdates
      rooms <- requestJSON $ getWith opts url
      rooms `shouldContain` [expectedAfterUpdates]

  describe "/rooms/:room_id/members" $
    it "should initially have no members" $ \env -> do
      let create = RoomInfo "Maryland"
          url = buildRoomListUrl env
          opts = superuserOpts env
      Room {roomId} <- requestJSON $ postWith opts url $ toJSON create
      let roomMembersUrl = buildRoomUrl env roomId <> "/members"
      members :: [User] <- requestJSON $ getWith opts roomMembersUrl
      members `shouldBe` []

  describe "/rooms/:room_id/membership" $
    aroundWith withAuthenticatedUser $ do
      it "should add the current user to a room" $ \env -> do
        let create = RoomInfo "Denver"
            url = buildRoomListUrl env
            opts = userOpts env
        Room {roomId} <- requestJSON $ postWith opts url $ toJSON create
        let roomMembershipUrl = buildRoomMembershipUrl env roomId
            roomMembersUrl = buildRoomMembersUrl env roomId
        postWith opts roomMembershipUrl BS.empty
        members <- requestJSON $ getWith opts roomMembersUrl
        members `shouldBe` [getUser env]

      it "should tolerate multiple room join attempts" $ \env -> do
        let create = RoomInfo "Bravo"
            url = buildRoomListUrl env
            opts = userOpts env
        Room {roomId} <- requestJSON $ postWith opts url $ toJSON create
        let roomMembershipUrl = buildRoomMembershipUrl env roomId
            roomMembersUrl = buildRoomMembersUrl env roomId
        postWith opts roomMembershipUrl BS.empty
        postWith opts roomMembershipUrl BS.empty
        members <- requestJSON $ getWith opts roomMembersUrl
        members `shouldBe` [getUser env]

      it "remove the current user from a room" $ \env -> do
        let create = RoomInfo "Charley"
            url = buildRoomListUrl env
            opts = userOpts env
        Room {roomId} <- requestJSON $ postWith opts url $ toJSON create
        let roomMembershipUrl = buildRoomMembershipUrl env roomId
            roomMembersUrl = buildRoomMembersUrl env roomId
        postWith opts roomMembershipUrl BS.empty
        deleteWith opts roomMembershipUrl
        members :: [User] <- requestJSON $ getWith opts roomMembersUrl
        members `shouldBe` []

  describe "/rooms/:room_id/messages" $
    aroundWith withAuthenticatedUser $ do
      it "should initially have no messages" $ \env -> do
        let create = RoomInfo "Echo"
            url = buildRoomListUrl env
            opts = userOpts env
        Room {roomId} <- requestJSON $ postWith opts url $ toJSON create
        let roomMessagesUrl = buildRoomMessagesUrl env roomId
        messages :: [Message] <- requestJSON $ getWith opts roomMessagesUrl
        messages `shouldBe` []

      it "should add messages and retrieve them in descending created order" $ \env -> do
        let create = RoomInfo "Bravo"
            url = buildRoomListUrl env
            opts = userOpts env
        Room {roomId} <- requestJSON $ postWith opts url $ toJSON create
        let roomMessagesUrl = buildRoomMessagesUrl env roomId
        message1 <- requestJSON $ postWith opts roomMessagesUrl $ toJSON $ MessageInfo "Hello..."
        message2 <- requestJSON $ postWith opts roomMessagesUrl $ toJSON $ MessageInfo "world!"
        messages :: [Message] <- requestJSON $ getWith opts roomMessagesUrl
        messages `shouldBe` [message2, message1]

buildRoomListUrl :: EnvHelpers e => e -> String
buildRoomListUrl = flip buildUrl "/rooms"

buildRoomUrl :: EnvHelpers e => e -> RoomId -> String
buildRoomUrl env (RoomId wrapped) = buildRoomListUrl env <> "/" <> show wrapped

buildRoomMembersUrl :: EnvHelpers e => e -> RoomId -> String
buildRoomMembersUrl env roomId = buildRoomUrl env roomId <> "/members"

buildRoomMembershipUrl :: EnvHelpers e => e -> RoomId -> String
buildRoomMembershipUrl env roomId = buildRoomUrl env roomId <> "/membership"

buildRoomMessagesUrl :: EnvHelpers e => e -> RoomId -> String
buildRoomMessagesUrl env roomId = buildRoomUrl env roomId <> "/messages"
